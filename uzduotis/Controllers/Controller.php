<?php
namespace Controllers;

class Controller{        
        /**
         * CreateView
         *
         * @param  mixed $viewName
         * @return void
         */
        public static function CreateView($viewName){
            require_once("./Views/$viewName.php");
        }
}

?>