<?php
    spl_autoload_register(function($className) 
    {
        include dirname(__FILE__) . '\\' . str_replace('\\', "/", $className) . '.php';
    });
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Užduotis</title>
<link rel="stylesheet" href="public/css/style.css">
</head>
<body>

<?php
require_once('Classes/Routes.php');
?>
</body>
</html>