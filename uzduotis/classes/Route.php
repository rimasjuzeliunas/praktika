<?php
namespace Classes;

class Route{

    public static $validRoutes = array();
    
    /**
     * set
     *
     * @param  mixed $route
     * @param  mixed $function
     * @return void
     */
    public static function set($route,$function){

        self::$validRoutes[] = $route;

        if($_GET['url']==$route){
            $function->__invoke();
        }
    }
}
?>